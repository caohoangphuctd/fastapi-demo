.PHONY: deploy-staging all-staging deploy-live all-live login

deploy-staging:
	@kubectl set image deployment/interactive interactive=${CONTAINER_RELEASE_IMAGE} -n interactive-service

all-staging: build deploy-staging

deploy-live:
	@kubectl set image deployment/interactive interactive=${INTERACTIVE} -n interactive-service --context=do-sgp1-production --record

all-live: build deploy-live

login:
	@docker login registry.gitlab.com

run-local:
	FLASK_APP=app.manage:flask_app python -m flask run

.PHONY: run-local
